const r = require('rethinkdb');

const connect = async () => {
  return r.connect({ host: 'localhost', port: 28015 });
};
module.exports = connect;
