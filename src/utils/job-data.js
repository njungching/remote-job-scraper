const jobData = (data) => {
  const { company, jobTitle, datePosted, jobType, location, linkToApply } = data;
  return {
    company,
    jobTitle,
    datePosted,
    jobType,
    location,
    linkToApply
  };
};
export default jobData;
