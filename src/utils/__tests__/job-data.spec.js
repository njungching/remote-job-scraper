import jobData from '../job-data';

describe('Job data module', () => {
  const company = '';
  const jobTitle = '';
  const datePosted = '';
  const jobType = '';
  const location = '';
  const linkToApply = '';
  test('Should return an object', () => {
    const result = jobData({});
    expect(typeof result).toBe('object');
  });
  test('Variable names of properties passed should be similar to those in the returned object', () => {
    const result = jobData({ company, jobTitle, datePosted, jobType, location, linkToApply });
    Object.keys(result).forEach((key) => {
      expect(typeof result[key]).not.toBe('undefined');
    });
  });
  test('The returned object should contain the correct keys', () => {
    const result = jobData({});
    expect(result).toHaveProperty('company');
    expect(result).toHaveProperty('jobTitle');
    expect(result).toHaveProperty('datePosted');
    expect(result).toHaveProperty('jobType');
    expect(result).toHaveProperty('location');
    expect(result).toHaveProperty('linkToApply');
  });
});
