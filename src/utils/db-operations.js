import r from 'rethinkdb';

const connect = require('./db');

export const selectJobs = async () => {
  const connection = await connect();
  return r
    .db('remote_jobs_test')
    .table('jobs')
    .run(connection)
    .then((cursor) => {
      return cursor.toArray();
    });
};

export const saveJobs = async (jobs) => {
  const connection = await connect();
  return r
    .db('remote_jobs_test')
    .table('jobs')
    .insert(jobs)
    .run(connection, (err, result) => {
      if (err) {
        console.log(err);
        return;
      }
      console.log(result);
    });
};
