const blackList = [
  'US',
  'U.S',
  'States',
  'America',
  'Australia',
  'Canada',
  'P.S.T',
  'M.S.T',
  'C.S.T',
  'PST',
  'CST',
  'EST',
  'NYC',
  'CA',
  'NC',
  'FL',
  'New England'
];
export default blackList;
