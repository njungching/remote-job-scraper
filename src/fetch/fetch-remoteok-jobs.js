import rp from 'request-promise';
import $ from 'cheerio';
import { remoteOkDetails } from '../details/remoteok-details';

const url = 'https://remoteok.io/remote-dev-jobs';
export const getJobsPostedLastThirtyDays = (firstHeading, secondHeading, html) => {
  let result = [];
  if (firstHeading === 'Today' && secondHeading === 'Yesterday') {
    result = remoteOkDetails($('#jobsboard tbody', html).eq(3));
  }
  return result;
};
/**
 * fetchRemoteOkJobs
 * * Retrieve jobs from 'https://remoteok.io
 * @returns {Promise.<result>} result is an array containing all the retrieved jobs
 */
export const fetchRemoteOkJobs = () => {
  return rp(url).then((html) => {
    const firstHeading = $('#jobsboard thead .heading', html)
      .eq(0)
      .text()
      .trim();
    const secondHeading = $('#jobsboard thead .heading', html)
      .eq(1)
      .text()
      .trim();
    const jobsPostedToday = remoteOkDetails($('#jobsboard tbody', html).eq(0));
    const jobsPostedYesterday = remoteOkDetails($('#jobsboard tbody', html).eq(1));
    const jobsPostedLastSevenDays = remoteOkDetails($('#jobsboard tbody', html).eq(2));
    const jobsPostedLastThirtyDays = getJobsPostedLastThirtyDays(firstHeading, secondHeading, html);

    return [...jobsPostedToday, ...jobsPostedYesterday, ...jobsPostedLastSevenDays, ...jobsPostedLastThirtyDays];
  });
};
export default fetchRemoteOkJobs;
