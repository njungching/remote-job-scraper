import rp from 'request-promise';
import fs from 'fs';
import util from 'util';
import { fetchRemoteOkJobs, getJobsPostedLastThirtyDays } from '../fetch-remoteok-jobs';

const file = 'src/__mocks__/remote_ok';
const readFile = util.promisify(fs.readFile);
describe('Test fetching remoteok jobs', () => {
  let html;
  let remoteOkJobs;
  beforeAll(async () => {
    html = await readFile(file, 'utf8');
    rp.mockImplementationOnce(() => Promise.resolve(html));
    remoteOkJobs = await fetchRemoteOkJobs();
  });
  test('should return an object', async () => {
    expect(typeof remoteOkJobs).toBe('object');
  });
  test('sould contain company name', () => {
    remoteOkJobs.forEach((job) => {
      expect(job.company).toBeTruthy();
    });
  });
  test('should contain the job title', () => {
    remoteOkJobs.forEach((job) => {
      expect(job.jobTitle).toBeTruthy();
    });
  });
  test('sould contain a link to apply', () => {
    remoteOkJobs.forEach((job) => {
      expect(job.linkToApply).toBeTruthy();
    });
  });
  test('should return an array of jobs', () => {
    const jobs = getJobsPostedLastThirtyDays('Today', 'Yesterday', html);
    expect(jobs.length).toBeGreaterThan(0);
  });
  test('should return an empty array', () => {
    const jobs = getJobsPostedLastThirtyDays('Last', 'Last', html);
    expect(jobs.length).toEqual(0);
  });
});
