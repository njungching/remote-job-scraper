import rp from 'request-promise';
import fs from 'fs';
import util from 'util';
import { fetchWeWorkRemotelyJobs, getDatePosted } from '../fetch-we-work-remotely-jobs';

const file = 'src/__mocks__/we_work_remotely';
const readFile = util.promisify(fs.readFile);
describe('Test fetching WWR jobs', () => {
  let html;
  let weWorkRemotelyJobs;
  beforeAll(async () => {
    html = await readFile(file, 'utf8');
    rp.mockImplementationOnce(() => Promise.resolve(html));
    weWorkRemotelyJobs = await fetchWeWorkRemotelyJobs();
  });
  test('should return an object', async () => {
    expect(typeof weWorkRemotelyJobs).toBe('object');
  });
  test('sould contain company name', () => {
    weWorkRemotelyJobs.forEach((job) => {
      expect(job.company).toBeTruthy();
    });
  });
  test('should contain the job title', () => {
    weWorkRemotelyJobs.forEach((job) => {
      expect(job.jobTitle).toBeTruthy();
    });
  });
  test('sould contain a link to apply', () => {
    weWorkRemotelyJobs.forEach((job) => {
      expect(job.linkToApply).toBeTruthy();
    });
  });
  test('no date input should return current date', () => {
    const date = new Date();
    const datePosted = getDatePosted('');
    expect(datePosted).toStrictEqual(date);
  });
  test('valid date input should return the correct timestamp', () => {
    const date = new Date('2019-10-20');
    date.setDate(date.getDate());
    const datePosted = getDatePosted('Oct 20');
    expect(datePosted).toStrictEqual(date);
  });
});
