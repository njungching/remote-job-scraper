import rp from 'request-promise';
import fs from 'fs';
import util from 'util';
import { fetchRemotiveJobs, getDatePosted } from '../fetch-remotive-jobs';

const file = 'src/__mocks__/remotive';
const readFile = util.promisify(fs.readFile);
describe('Test fetching remotive jobs', () => {
  let html;
  let remotiveJobs;
  beforeAll(async () => {
    html = await readFile(file, 'utf8');
    rp.mockImplementationOnce(() => Promise.resolve(html));
    remotiveJobs = await fetchRemotiveJobs();
  });
  test('should return an object', async () => {
    expect(typeof remotiveJobs).toBe('object');
  });
  test('sould contain company name', () => {
    remotiveJobs.forEach((job) => {
      expect(job.company).toBeTruthy();
    });
  });
  test('should contain the job title', () => {
    remotiveJobs.forEach((job) => {
      expect(job.jobTitle).toBeTruthy();
    });
  });
  test('sould contain a link to apply', () => {
    remotiveJobs.forEach((job) => {
      expect(job.linkToApply).toBeTruthy();
    });
  });
  test("job posted today should return today's date", () => {
    const date = new Date();
    date.setDate(date.getDate());
    const input = ['Today'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test("job posted yesterday should return yesterday's timestamp", () => {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    const input = ['Yesterday'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 2 days ago should return the timestamp for 2 days ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 2);
    const input = ['2', 'days', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 3 days ago should return the timestamp for 3 days ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 3);
    const input = ['3', 'days', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 4 days ago should return the timestamp for 4 days ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 4);
    const input = ['4', 'days', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 5 days ago should return the timestamp for 5 days ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 5);
    const input = ['5', 'days', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 6 days ago should return the timestamp for 6 days ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 6);
    const input = ['6', 'days', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted a week ago should return the timestamp for a week ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 7);
    const input = ['1', 'week', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 2 weeks ago should return the timestamp for 2 weeks ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 14);
    const input = ['2', 'weeks', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 3 weeks ago should return the timestamp for 2 weeks ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 21);
    const input = ['3', 'weeks', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 4 weeks ago should return the timestamp for 4 weeks ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 28);
    const input = ['4', 'weeks', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 1 month ago should return the timestamp for 1 month ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 30);
    const input = ['1', 'month', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted 2 months ago should return the timestamp for 2 months ago', () => {
    const date = new Date();
    date.setDate(date.getDate() - 60);
    const input = ['2', 'months', 'ago'];
    const datePosted = getDatePosted(input);
    expect(datePosted).toStrictEqual(date);
  });
});
