import rp from 'request-promise';
import $ from 'cheerio';
import jobData from '../utils/job-data';

const url = 'https://weworkremotely.com/categories/remote-programming-jobs';
const months = {
  Jan: 0,
  Feb: 1,
  Mar: 2,
  Apr: 3,
  May: 4,
  Jun: 5,
  Jul: 6,
  Aug: 7,
  Sep: 8,
  Oct: 9,
  Nov: 10,
  Dec: 11
};
export const getDatePosted = (date) => {
  let datePosted = new Date();
  if (!date) {
    datePosted.setDate(datePosted.getDate());
  } else {
    const dateInput = date.split(' ');
    const year = new Date().getFullYear();
    const month = months[dateInput[0]] + 1;
    const day = dateInput[1];
    datePosted = new Date(`${year}-${month}-${day}`);
    datePosted.setDate(datePosted.getDate());
  }
  return datePosted;
};
/**
 * fetchWeWorkRemotelyJobs
 * * Retrieve jobs from https://weworkremotely.com
 * @returns {Promise.<result>} result is an array containing all the retrieved jobs
 */
export const fetchWeWorkRemotelyJobs = () => {
  return rp(url).then((html) => {
    const jobs = $('article li', html);
    const jobListLength = jobs.length - 1;
    const result = [];
    for (let i = 0; i < jobListLength; i += 1) {
      const company = $('.company', jobs[i])
        .eq(0)
        .text();
      const jobTitle = $('.title', jobs[i]).text();
      const jobType = $('.company', jobs[i])
        .eq(1)
        .text();
      const datePosted = getDatePosted($('time', jobs[i]).text());
      const location = $('.region', jobs[i]).text();
      const linkToApply = `https://weworkremotely.com${jobs[i].attribs.href}`;
      result.push(
        jobData({
          company,
          jobTitle,
          datePosted,
          jobType,
          location,
          linkToApply
        })
      );
    }
    return result;
  });
};
