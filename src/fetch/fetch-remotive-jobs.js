import rp from 'request-promise';
import $ from 'cheerio';
import jobData from '../utils/job-data';

const url = 'https://remotive.io/remote-jobs/software-dev';
export const getDatePosted = (jobDate) => {
  const date = jobDate[1] ? jobDate[1] : '';
  const b = jobDate[0];
  const currentDate = new Date();
  if (b === 'Yesterday') {
    currentDate.setDate(currentDate.getDate() - 1);
  } else if (b === '2' && date === 'days') {
    currentDate.setDate(currentDate.getDate() - 2);
  } else if (b === '3' && date === 'days') {
    currentDate.setDate(currentDate.getDate() - 3);
  } else if (b === '4' && date === 'days') {
    currentDate.setDate(currentDate.getDate() - 4);
  } else if (b === '5' && date === 'days') {
    currentDate.setDate(currentDate.getDate() - 5);
  } else if (b === '6' && date === 'days') {
    currentDate.setDate(currentDate.getDate() - 6);
  } else if (b === '1' && date === 'week') {
    currentDate.setDate(currentDate.getDate() - 7);
  } else if (b === '2' && date === 'weeks') {
    currentDate.setDate(currentDate.getDate() - 14);
  } else if (b === '3' && date === 'weeks') {
    currentDate.setDate(currentDate.getDate() - 21);
  } else if (b === '4' && date === 'weeks') {
    currentDate.setDate(currentDate.getDate() - 28);
  } else if (b === '1' && date === 'month') {
    currentDate.setDate(currentDate.getDate() - 30);
  } else if (b === '2' && date === 'months') {
    currentDate.setDate(currentDate.getDate() - 60);
  } else {
    currentDate.setDate(currentDate.getDate());
  }
  return currentDate;
};
/**
 * fetchRemotiveJobs
 * * Retrieve jobs from https://remotive.io
 * @returns {Promise.<result>} result is an array containing all the retrieved jobs
 */
export const fetchRemotiveJobs = () => {
  return rp(url).then((html) => {
    const jobList = $('.job-list-item', html);
    const result = [];
    for (let i = 0; i < jobList.length; i += 1) {
      const company = $('.company span', jobList[i])
        .eq(0)
        .text();
      const jobTitle = $('.position', jobList[i])
        .text()
        .trim();
      const jobDate = $('.job-date', jobList[i])
        .text()
        .trim()
        .split(' ');
      const datePosted = getDatePosted(jobDate);
      const jobType = '';
      const location = $('.company span', jobList[i])
        .eq(1)
        .text()
        .trim();
      const linkToApply = `https://remotive.io${jobList[i].attribs['data-url']}`;
      result.push(
        jobData({
          company,
          jobTitle,
          datePosted,
          jobType,
          location,
          linkToApply
        })
      );
    }
    return result;
  });
};
export default fetchRemotiveJobs;
