/**
 * * This app fetches remote jobs from different sources and retrieves those available to EMEA
 * ? Many of the listed jobs don't provide region/location info making it difficult to filter, therefore these jobs will be included on the list
 * ! This app is heavily dependent on the sources, any change on the sources DOM structure will defintely break things
 * TODO: Insert jobs into database
 * TODO: Check for duplicates from the diffetent sources
 */
import { fetchWeWorkRemotelyJobs } from './fetch/fetch-we-work-remotely-jobs';
import { fetchRemotiveJobs } from './fetch/fetch-remotive-jobs';
import { fetchRemoteOkJobs } from './fetch/fetch-remoteok-jobs';
import blackList from './utils/black-list';
import whiteList from './utils/white-list';
import { selectJobs, saveJobs } from './utils/db-operations';

/**
 * filterEMEAJobs
 * * Where you can, filter out jobs that explicitly state they are not available to EMEA
 * @param {Array} result Array containing EMEA remote jobs
 */
export const filterEMEAJobs = (jobs) => {
  const result = [];
  const count = 0;
  jobs.forEach((job) => {
    if (!new RegExp(blackList.join('|')).test(job.location) || new RegExp(whiteList.join('|')).test(job.location)) {
      // console.log(`${count++} ${job.company}=${job.location}=${job.datePosted}`);
      result.push(job);
    }
  });
  return result;
};

/**
 * start
 * * Kick-start the process of fetching jobs from different sources
 */
export const start = async () => {
  // let jobs;
  try {
    const weWorkRemotelyJobs = await fetchWeWorkRemotelyJobs();
    const remotiveJobs = await fetchRemotiveJobs();
    const remoteOkJobs = await fetchRemoteOkJobs();
    const jobs = filterEMEAJobs([...weWorkRemotelyJobs, ...remotiveJobs, ...remoteOkJobs]);
    const result = await selectJobs();
    if (result) {
      // insert into database
      console.log('insert into database');
      await saveJobs(jobs);
    } else {
      // check for duplicates, get new jobs, insert these new jobs
    }
  } catch (err) {
    console.log(err);
  }
  // return jobs;
};
