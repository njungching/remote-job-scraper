const r = require('rethinkdb');
const connect = require('../utils/db');

const createDatabase = async () => {
  const connection = await connect();
  return { dbCreate: r.dbCreate('remote_jobs_test').run(connection), connection };
};
module.exports = createDatabase;
