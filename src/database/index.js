const createDatabase = require('../database/create-database');
const createTable = require('../database/create-table');

const handleCreateTableResult = (response) => {
  if (response.tables_created === 1) {
    console.log('Table created successfully');
  } else {
    console.log('Table was not created');
  }
};

const handleCreateDatabaseResult = (createDB, result) => {
  if (result.dbs_created === 1) {
    console.log('Database created successfully');
    createTable(createDB.connection)
      .then((response) => {
        handleCreateTableResult(response);
        createDB.connection.close();
      })
      .catch((err) => {
        console.log('Error occured creating table', err);
        createDB.connection.close();
      });
  } else {
    console.log('Database was not created');
    createDB.connection.close();
  }
};

const createDatabaseAndTable = async () => {
  try {
    const createDB = await createDatabase();
    createDB.dbCreate
      .then((result) => {
        handleCreateDatabaseResult(createDB, result);
      })
      .catch((err) => {
        console.log('Error occured creating database:', err);
        createDB.connection.close();
      });
  } catch (err) {
    console.log(err);
  }
};

createDatabaseAndTable();
