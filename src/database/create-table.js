const r = require('rethinkdb');

const createTable = (connection) => {
  return r
    .db('remote_jobs_test')
    .tableCreate('jobs')
    .run(connection);
};
module.exports = createTable;
