import $ from 'cheerio';
import jobData from '../utils/job-data';

export const getDatePosted = (date) => {
  const currentDate = new Date();
  const period = date[date.length - 1];
  if (period === 'h') {
    currentDate.setDate(currentDate.getDate());
  } else if (period === 'o') {
    currentDate.setDate(currentDate.getDate() - 30);
  } else {
    const days = date.slice(0, -1);
    currentDate.setDate(currentDate.getDate() - days);
  }
  return currentDate;
};

/**
 * remoteOkDetails
 * * Retrieve job details from https://remotive.io
 * @param {Array} Array containing job details
 */
export const remoteOkDetails = (html) => {
  const result = [];
  const jobRow = $('.job', html);
  for (let i = 0; i < jobRow.length; i += 1) {
    const company = $('.company_and_position h3', jobRow[i])
      .text()
      .trim();
    const jobTitle = $('.company_and_position h2', jobRow[i])
      .text()
      .trim();
    const jobType = '';
    const datePosted = getDatePosted($('.time', jobRow[i]).text());
    const location = $('td .location', jobRow[i])
      .text()
      .trim();
    const linkToApply = `https://remoteok.io${$('.company_and_position .preventLink', jobRow[i])[0].attribs.href}`;
    result.push(
      jobData({
        company,
        jobTitle,
        datePosted,
        jobType,
        location,
        linkToApply
      })
    );
  }
  return result;
};
export default remoteOkDetails;
