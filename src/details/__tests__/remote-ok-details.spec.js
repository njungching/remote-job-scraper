import $ from 'cheerio';
import fs from 'fs';
import util from 'util';
import { remoteOkDetails, getDatePosted } from '../remoteok-details';

const file = 'src/__mocks__/remote_ok';
const readFile = util.promisify(fs.readFile);

describe('remote ok details', () => {
  let html;
  beforeAll(async () => {
    html = await readFile(file, 'utf8');
  });
  test('Should return an object', () => {
    const result = remoteOkDetails($('#jobsboard tbody', html).eq(2));
    expect(typeof result).toBe('object');
  });
  test('jobs posted within hours, should return current date', () => {
    const date = new Date();
    date.setDate(date.getDate());
    const datePosted = getDatePosted('6h');
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted a month ago, should return current date - 30 days', () => {
    const date = new Date();
    date.setDate(date.getDate() - 30);
    const datePosted = getDatePosted('1mo');
    expect(datePosted).toStrictEqual(date);
  });
  test('jobs posted within provided date, should return correct timestamp', () => {
    const date = new Date();
    date.setDate(date.getDate() - 1);
    const datePosted = getDatePosted('1d');
    expect(datePosted).toStrictEqual(date);
  });
});
