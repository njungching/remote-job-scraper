[![codebeat badge](https://codebeat.co/badges/e742f290-77b5-4723-a8a8-1dd3cdfbc89c)](https://codebeat.co/projects/gitlab-com-njungching-remote-job-scraper-master)
[![pipeline status](https://gitlab.com/njungching/remote-job-scraper/badges/master/pipeline.svg)](https://gitlab.com/njungching/remote-job-scraper/commits/master)
[![coverage report](https://gitlab.com/njungching/remote-job-scraper/badges/master/coverage.svg)](https://gitlab.com/njungching/remote-job-scraper/commits/master)
# remote-job-scraper

Tiny app that fetches a list of EMEA remote jobs available on the web.